package com.wen.compilerandruntime;

/*
* debug探索JAVA多态中的隐藏和覆盖
*
* 原理:
* Java的引用变量有两个类型:
* 编译时类型：由声明该变量时使用的类型决定
* 运行时类型：由该变量指向的对象类型决定
*
*
* 结论:
* 获取static成员变量、成员变量、static方法使用的是Father中的类型
* 获取方法使用的是Son中的类型
* */
public class Father {
    int i=0;
    Father(){
        System.out.println("gou zao");
    }
    public static void hi(){
        System.out.println("hi, father");
    }
}

class son extends Father{
    int i = 1;
    public static void hi(){
        System.out.println("hi, son");
    }
}

class main{
    public static void main(String[] args) {
        Father f = new son();
        System.out.println(f.i);
        f.hi();

//        Father f = new Father();
//        son s = (son)f;
//        System.out.println(s.i);
//        s.hi();
    }
}
